package com.techprimers.springbootsoapexample.service;

import com.techprimers.spring_boot_soap_example.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {

    private static final Map<String, User> users = new HashMap<>();

    @PostConstruct
    public void initialize() {

        User felipe = new User();
        felipe.setName("Maria");
        felipe.setSexo("Feminino");
        felipe.setEmpId(1111);
        felipe.setSalary(12000);

        User pedro = new User();
        pedro.setName("Pedro");
        pedro.setSexo("Masculino");
        pedro.setEmpId(1112);
        pedro.setSalary(32000);

        User joao = new User();
        joao.setName("João");
        joao.setSexo("Masculino");
        joao.setEmpId(1113);
        joao.setSalary(16000);

        users.put(felipe.getName(), felipe);
        users.put(pedro.getName(), pedro);
        users.put(joao.getName(), joao);
    }


    public User getUsers(String name) {
        return users.get(name);
    }
}
