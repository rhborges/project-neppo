package br.com.curso.neppo.neppo.service.exception;

public class AlunoNotFoundException extends RuntimeException {

    public AlunoNotFoundException(String message) {
        super(message);
    }

    public AlunoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlunoNotFoundException(Throwable cause) {
        super(cause);
    }
}
