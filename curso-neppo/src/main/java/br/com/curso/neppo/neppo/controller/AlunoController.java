package br.com.curso.neppo.neppo.controller;

import br.com.curso.neppo.neppo.model.Aluno;
import br.com.curso.neppo.neppo.service.AlunoService;
import br.com.curso.neppo.neppo.service.exception.AlunoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/aluno")
public class AlunoController {

    @Autowired
    private AlunoService alunoService;

    @GetMapping
    public ResponseEntity findAll(){
        return ResponseEntity.ok( alunoService.findAll()  );
    }

    @GetMapping("/{id}")
    public ResponseEntity findOne(@PathVariable Long id){
        return ResponseEntity.ok( alunoService.findOne(id)  );
    }

    @PostMapping
    public ResponseEntity save (@RequestBody Aluno aluno){
        try {
            return ResponseEntity.ok(alunoService.save(aluno));
        } catch ( AlunoNotFoundException e ){
            return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
        }
    }

    @PutMapping
    public ResponseEntity update (@RequestBody Aluno aluno){
        try {
            return ResponseEntity.ok(alunoService.save(aluno));
        } catch ( AlunoNotFoundException e ){
            return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete (@PathVariable Long id){
        try {
            alunoService.delete(id);
            return ResponseEntity.ok().build( );
        } catch ( AlunoNotFoundException e ){
            return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
        }
    }
}
