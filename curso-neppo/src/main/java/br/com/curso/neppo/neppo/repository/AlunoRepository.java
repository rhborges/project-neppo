package br.com.curso.neppo.neppo.repository;

import br.com.curso.neppo.neppo.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlunoRepository extends JpaRepository<Aluno, Long > {

    List<Aluno> findAll();

    Aluno findOne(Long id);

    Aluno save(Aluno brand);

    void delete(Long id);
}
