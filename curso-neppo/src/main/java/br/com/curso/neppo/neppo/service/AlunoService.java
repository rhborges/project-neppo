package br.com.curso.neppo.neppo.service;


import br.com.curso.neppo.neppo.model.Aluno;

import java.util.List;

public interface AlunoService {

    List<Aluno> findAll();

    Aluno findOne(Long id);

    Aluno save(Aluno aluno);

    void delete(Long id);
}
