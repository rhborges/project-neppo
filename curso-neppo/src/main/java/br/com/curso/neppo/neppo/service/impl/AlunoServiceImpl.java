package br.com.curso.neppo.neppo.service.impl;


import br.com.curso.neppo.neppo.model.Aluno;
import br.com.curso.neppo.neppo.repository.AlunoRepository;
import br.com.curso.neppo.neppo.service.AlunoService;
import br.com.curso.neppo.neppo.service.exception.AlunoNotFoundException;
import br.com.curso.neppo.neppo.service.exception.InvalidParametersException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AlunoServiceImpl implements AlunoService {

    @Autowired
    private AlunoRepository alunoRepository;

    @Override
    public List<Aluno> findAll () {
        return this.alunoRepository.findAll();
    }

    @Override
    public Aluno findOne(Long id){

        if( id == null){
            throw new InvalidParametersException("Id cannot be null !!");
        }
        return this.alunoRepository.findOne(id);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public Aluno save (Aluno aluno) {
        if(aluno == null ){
            throw new AlunoNotFoundException("Aluno cannot be null !!");
        }

        return this.alunoRepository.save(aluno);
    }

    @Override
    public void delete( Long id ) {
        this.alunoRepository.delete(id);
    }
}
